<?php
/**
 * Created by PhpStorm.
 * User: rwrz
 * Date: 18/08/2016
 * Time: 10:07
 */


namespace CelulaLib\DataTable\Action;
use CelulaAcl\Service\Acl;

/**
 *
 */
class Action
{
    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var array
     */
    protected $dbColumns = array();

    /**
     * @var string
     */
    protected $resource = '';

    /**
     * @var string
     */
    protected $privilege = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var Acl
     */
    protected $aclService = null;

    /**
     * Array with dbColumn and value keys, to be validated (on a equals), or a closure that returns bool
     * @var bool|array|\Closure
     */
    protected $requirement = false;

    /**
     * Creates a DataTable Action to be used on Elements as Smart URLs
     * @param $name
     * @param $url
     * @param array $dbColumns
     * @param string $resource
     * @param string $privilege
     * @param bool|array|\Closure $requirement Return value to print only if they meet the requirement represented by this variable this array should has 3 keys: dbColumn, value and validateFunction (\Closure)
     * @param Acl $aclService
     * @throws \Exception
     */
    public function __construct($name, $url, $dbColumns = array(), $resource = '', $privilege = '', $requirement = false, Acl $aclService)
    {
        if (empty($name) || empty($url)) {
            throw new \Exception("Action fields 'name' and 'url' cannot be blank.");
        }

        $this->setAclService($aclService);
        $this->setName($name);
        $this->setUrl($url);
        $this->setDbColumns($dbColumns);
        $this->setResource($resource);
        $this->setPrivilege($privilege);
        $this->setRequirement($requirement);
    }

    /**
     * @return array|bool|\Closure
     */
    public function getRequirement()
    {
        return $this->requirement;
    }

    /**
     * @param array|bool|\Closure $requirement
     */
    public function setRequirement($requirement)
    {
        $this->requirement = $requirement;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getDbColumns()
    {
        return $this->dbColumns;
    }

    /**
     * @param array $dbColumns
     */
    public function setDbColumns($dbColumns)
    {
        $this->dbColumns = $dbColumns;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return string
     */
    public function getPrivilege()
    {
        return $this->privilege;
    }

    /**
     * @param string $privilege
     */
    public function setPrivilege($privilege)
    {
        $this->privilege = $privilege;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Acl
     */
    public function getAclService()
    {
        return $this->aclService;
    }

    /**
     * @param Acl $aclService
     */
    public function setAclService($aclService)
    {
        $this->aclService = $aclService;
    }

    public function isAllowed()
    {
        if (empty($this->getResource()) || empty($this->getPrivilege())) {
            return true;
        }

        return $this->getAclService()->isAllowed($this->getResource(), $this->getPrivilege());
    }
}