<?php

namespace CelulaLib\DataTable\Column;

use CelulaLib\DataTable\Action\Action;
use CelulaLib\DataTable\Element\DbElement\Element as DbElement;
use CelulaLib\DataTable\Element\HtmlElement\AbstractElement;
use CelulaLib\DataTable\Element\InterfaceElement;

/**
 *
 */
class Column
{
    /**
     * @var Integer
     */
    private $index;

    /**
     * @var String
     */
    private $separator;
    /**
     * @var InterfaceElement[]
     */
    private $elements = array();
    /**
     * @var bool
     */
    private $enableSorting = false;

    /**
     * Em pixels
     * @var int
     */
    private $width = "";

    /**
     * Constructor
     */
    public function __construct($index)
    {
        $this->index = $index;
    }

    /**
     * @param $element
     */
    public function insertElement($element)
    {
        if ($this->elementInstanceOfChecks($element)) {
            $this->elements[] = $element;
        }
    }

    public function clearElements()
    {
        $this->elements = array();
    }

    /**
     * @param $element
     * @return bool
     * @throws \Exception
     */
    public function elementInstanceOfChecks($element)
    {
        if ($element instanceof InterfaceElement) {
            return true;
        } else {
            throw new \Exception('The element must be an implementation of InterfaceElement');
        }
    }

    /**
     * @param $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @return mixed
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param $line
     * @param Action[] $actions
     * @return string
     */
    public function getValueToPrint($line, $actions)
    {
        $value = "";
        $elementPrint = null;
        foreach ($this->elements as $element) {
            if (!empty($this->separator) && !empty($elementPrint)) {
                $value .= $this->separator;
            }
            $elementPrint = $element->getValueToPrint($line, $actions);
            $value .= $elementPrint;
        }

        return $value;
    }

    /**
     * @return array
     */
    public function getAllDbElements()
    {
        $filterValues = array();
        foreach ($this->elements as $element) {
            if ($element instanceof DbElement) {
                $filterValues[] = $element;
            }
        }
        return $filterValues;
    }

    /**
     * @return \Celula\CelulaDataTable\Element\InterfaceElement[]|AbstractElement[]|DbElement[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param boolean $enableSorting
     */
    public function setEnableSorting($enableSorting)
    {
        $this->enableSorting = $enableSorting;
    }

    /**
     * @return boolean
     */
    public function getEnableSorting()
    {
        return $this->enableSorting;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}
