<?php

namespace CelulaLib\DataTable;

use Application\Db\Sql\ModelSelect;
use CelulaAcl\Service\Acl;
use CelulaLib\DataTable\Action\Action;
use CelulaLib\DataTable\Column\Column;
use CelulaLib\DataTable\Column\ColumnContainer;
use CelulaLib\DataTable\Element\DbElement\Element as DbElement;
use CelulaLib\DataTable\Element\InterfaceElement;
use CelulaLib\DataTable\Filter\FilterContainer;
use CelulaLib\DataTable\Filter\HtmlInputFilter\AbstractFilter;
use CelulaLib\DataTable\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use CelulaLib\Utils;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Paginator\Paginator;

/**
 * Class DataTable
 * @package CelulaLib\DataTable
 */
class DataTable implements EventManagerAwareInterface
{
    /**
     * @var ColumnContainer
     */
    protected $columnContainer;
    /**
     * @var Filter\FilterContainer
     */
    protected $filterContainer;
    /**
     * @var array;
     */
    protected $params;

    /**
     * @var Action[]
     */
    protected $actions = array();

    /**
     * @var Acl
     */
    protected $aclService = null;

    /**
     * @var
     */
    protected $events;

    /**
     * @var array
     */
    protected $sorting = ""; //array("1" => "asc");

    protected $paginationType = "full_numbers";
    protected $sDom = "<ipl>rt<p>";
    protected $oPaginate = "'sFirst': '', 'sPrevious': '', 'sNext': '', 'sLast': ''";
    protected $sInfo         = '<a class="tooltip" title="Download do resultado em planilha" href="#" onclick="gerarRelatorioDatatable(\\\'_URL_\\\', {\\\'report\\\': 1})"><img src="/gfx/icon-excel.png"/></a><span class="tooltip datatableTotal" title="Total de Registros">_TOTAL_</span>';
    protected $sInfoEmpty    = '<span class="tooltip datatableTotal" title="Total de Registros">_TOTAL_</span>';

    /**
     * DataTable constructor.
     * @param Acl $aclService
     */
    public function __construct(Acl $aclService)
    {
        $this->aclService = $aclService;
        $this->columnContainer = new ColumnContainer();
        $this->filterContainer = new FilterContainer();
    }

    /**
     * Array populado por array[columKey] = asc ou desc
     * @param array $array
     */
    public function setSortingOrder($array = array())
    {
        $this->sorting = $array;
    }

    /**
     * @param array $params
     */
    public function updateParams($params = array())
    {
        $this->params = $params;
    }

    /**
     * @param $nameOrAction
     * @param string $url
     * @param array $dbColumns
     * @param string $resource
     * @param string $privilege
     * @param bool $requirement
     * @throws \Exception
     */
    public function addAction($nameOrAction, $url = '', $dbColumns = array(), $resource = '', $privilege = '', $requirement = false)
    {
        $action = null;
        if ($nameOrAction instanceof Action) {
            $action = $nameOrAction;
        } else {
            if (empty($url)) {
                throw new \Exception("URL é parâmetro obrigatório ao se criar uma Action");
            }
            $action = new Action($nameOrAction, $url, $dbColumns, $resource, $privilege, $requirement, $this->aclService);
        }

        if (!$this->existAction($action)) {
            $this->actions[] = $action;
        }
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function isActionsAllowed()
    {
        foreach ($this->actions as $action) {
            if (!$action->isAllowed()) {
                return false;
            }
        }

        return true;
    }

    public function existAction(Action $newAction)
    {
        foreach ($this->actions as $action) {
            if ($action->getUrl() == $newAction->getUrl()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Filtra a SELECT de acordo com os filtros enviados pelo DataTable
     * @param $select
     */
    protected function filterSelect($select)
    {
        /* Busca em todos os filtros */
        $searchAll = false;
        $searchAllParams = "";
        $predicateSet = new \Zend\Db\Sql\Predicate\Predicate();
        $predicate = $predicateSet->nest();
        if (!empty($this->params["sSearch"])) {
            $searchAll = true;
            $searchAllParams = urldecode($this->params["sSearch"]);
        }

        foreach ($this->filterContainer->getFilters() as $filter) {
            if (empty($this->params[$filter->getIdentifier()]) && !$searchAll) {
                continue;
            }

            // buscaTudo não aceita bool
            if (($filter->getDataType() == AbstractFilter::BOOLEAN) && (!$searchAll)) {
                switch ($this->params[$filter->getIdentifier()]) {
                    case 'true':
                        $select->where(" ( ".$filter->getDbTable().".".$filter->getDbColumnName()." is TRUE ) ");
                        break;
                    case 'false':
                        $select->where(" ( ".$filter->getDbTable().".".$filter->getDbColumnName()." is FALSE ) ");
                        break;
                }
            } elseif ($filter->getDataType() == AbstractFilter::NUMERIC) {
                if ($searchAll) {
                    $predicate->addPredicate(
                        new \Zend\Db\Sql\Predicate\Operator(
                            $filter->getDbTable().".".$filter->getDbColumnName(),
                            \Zend\Db\Sql\Predicate\Operator::OP_EQ,
                            $searchAllParams
                        ),
                        \Zend\Db\Sql\Predicate\PredicateSet::OP_OR
                    );
                } else {
                    $select->where(
                        array(
                            $filter->getDbTable().".".$filter->getDbColumnName()
                            => $this->params[$filter->getIdentifier()]
                        )
                    );
                }
            } elseif ($filter->getDataType() == AbstractFilter::STRING) {
                if ($searchAll) {
                    $predicate->addPredicate(
                        new \Zend\Db\Sql\Predicate\Expression(
                            "sem_acentos(lower(".$filter->getDbTable().".".$filter->getDbColumnName().")) LIKE ?",
                            "%".Utils::removeAcentos(
                                mb_strtolower($searchAllParams, "UTF8")
                            )."%"
                        ),
                        \Zend\Db\Sql\Predicate\PredicateSet::OP_OR
                    );
                } else {
                    $select->where(
                        array(
                            new \Zend\Db\Sql\Predicate\Expression(
                                "sem_acentos(lower(".$filter->getDbTable().".".$filter->getDbColumnName().")) LIKE ?",
                                "%".Utils::removeAcentos(
                                    mb_strtolower($this->params[$filter->getIdentifier()], "UTF8")
                                )."%"
                            )
                        )
                    );
                }
            } elseif ($filter->getDataType() == AbstractFilter::DATE) {
                if ($searchAll) {
                    $predicate->addPredicate(
                        new \Zend\Db\Sql\Predicate\Expression(
                            "".$filter->getDbTable().".".$filter->getDbColumnName()." = ?",
                            "%".Utils::dateToBd($this->params[$filter->getIdentifier()])."%"
                        ),
                        \Zend\Db\Sql\Predicate\PredicateSet::OP_OR
                    );
                } else {
                    $select->where(
                        array(
                            new \Zend\Db\Sql\Predicate\Expression(
                                "".$filter->getDbTable().".".$filter->getDbColumnName()." = ?",
                                "%".Utils::dateToBd($this->params[$filter->getIdentifier()])."%"
                            )
                        )
                    );
                }
            } elseif (($filter->getDataType() == AbstractFilter::NULL) && (!$searchAll)) {
                switch($this->params[$filter->getIdentifier()]) {
                    case 'true':
                    case 'notnull':
                        $select->where(" ( ".$filter->getDbTable().".".$filter->getDbColumnName()." is NOT NULL ) ");
                        break;
                    case 'false':
                    case 'null':
                        $select->where(" ( ".$filter->getDbTable().".".$filter->getDbColumnName()." is NULL ) ");
                        break;
                }
            }
        }

        if ($predicate->count() > 0) {
            $select->where(array($predicateSet), \Zend\Db\Sql\Predicate\PredicateSet::OP_AND);
        }

        if (isset($this->params['iSortCol_0']) && isset($this->params['sSortDir_0'])) {
            $columnToSort = $this->columnContainer->getColumnByIndex((int) $this->params['iSortCol_0']);
            $columnElements = $columnToSort->getAllDbElements();
            /**
             * @var $element DbElement
             */
            foreach ($columnElements as $element) {
                $order = $select->getRawState(ModelSelect::ORDER);
                $select->reset(ModelSelect::ORDER);
                $select->order($element->getDbColumnAlias() . " " . $this->params['sSortDir_0']);
                $select->order($order);
            }
        }
    }

    /**
     * @param \Zend\Db\TableGateway\AbstractTableGateway $model
     * @param \Application\Db\Sql\ModelSelect $select
     * @return array
     */
    public function getOutput(AbstractTableGateway $model, $select = null, $forcaOrdemSelect = false)
    {
        //Creating the ZEND_DB_SELECT to set on the Paginator
        try {
            if (empty($select)) {
                $select = $model->getSql()->select();
            }

            if ($forcaOrdemSelect) {
                $model->getAdapter()->query("SET enable_nestloop = off")->execute();
            }

            // Filtra o SELECT
            $this->filterSelect($select);

            // Executa funções externas pos-filterSelect
            $this->getEventManager()->trigger("getOutput_afterFilterSelect", $this, compact('model', 'select', 'forcaOrdemSelect'));

            // Paginando o SELECT com o Zend_Paginator
            $paginatorSelect = clone $select;
            $paginator = new Paginator(new PaginatorDbSelectAdapter($paginatorSelect, $model->getAdapter()));
            if (isset($this->params['iDisplayLength'])) {
                $paginator->setItemCountPerPage((integer)$this->params['iDisplayLength']);

                if (isset($this->params['iDisplayLength'])) {
                    $paginator->setCurrentPageNumber(
                        (int)$this->params['iDisplayStart'] / (int)$this->params['iDisplayLength'] + 1
                    );
                }
            }

            // Creating Output['aaData']
            $output = array();
            $output['aaData'] = array();
            foreach ($paginator->getCurrentItems() as $line) {
                $valuesInLine = array();
                foreach ($this->columnContainer->getColumns() as $column) {
                    $valuesInLine[] = $column->getValueToPrint($line, $this->actions);
                }

                $output['aaData'][] = $valuesInLine;
            }

            // Getting params from paginator
            $output['iTotalRecords'] = $paginator->getTotalItemCount();
            $output['iTotalDisplayRecords'] = $paginator->getTotalItemCount();

            if ($forcaOrdemSelect) {
                $model->getAdapter()->query("SET enable_nestloop = on")->execute();
            }
        } catch (\Exception $exp) {
            $output['error'] = $exp->getMessage();

            if ($exp->getPrevious() !== null) {
                $output['error'] .= $exp->getPrevious()->getMessage();
            }

            if ($forcaOrdemSelect) {
                $model->getAdapter()->query("SET enable_nestloop = on")->execute();
            }
        }

        return $output;
    }

    /**
     * @param $url
     * @param $htmlTableId
     * @param null $filterButtonId
     * @param null $clearButtonId
     * @param string $extraParams
     * @param int $displayLength
     * @return string
     */
    public function renderJqueryDataTable(
        $url,
        $htmlTableId,
        $filterButtonId = null,
        $clearButtonId = null,
        $extraParams = "",
        $displayLength = 10,
        $saveState = true
    ) {

        // Quanto temos parâmetros no link, o DATATABLE coloca isso no nome do cookie...
        // fazendo com que temos VARIOS cookies para um mesmo PATH
        // essa rotina limpa isso
        /*
        $data = explode("/", $zend_action);

        if (count($data) <= 1) {
            $uri = str_replace("/".$zend_controller."/", "", $_SERVER["REQUEST_URI"]); // retira o controller
            $data = explode("/", $uri);
        }

        if (count($data) > 1) {
            $param = "ged_datatable_datatable_".$data[1];
            $path = "/".$zend_controller."/";

            foreach ($_COOKIE as $cookieKey => $values) {
                if ((stripos($cookieKey, "ged_datatable_datatable") !== false) && ($cookieKey != $param)) {
                    //print_r($values);
                    setcookie($cookieKey, "", time()-3600, $path);
                }
            }
        }

        <ul class="pagination">
            <li class="paginate_button previous disabled" id="datatable_previous">
                <a href="#" aria-controls="datatable" data-dt-idx="0" tabindex="0">Anterior</a></li>
                <li class="paginate_button active"><a href="#" aria-controls="datatable" data-dt-idx="1" tabindex="0">1</a></li>
                <li class="paginate_button next disabled" id="datatable_next"><a href="#" aria-controls="datatable" data-dt-idx="2" tabindex="0">Próximo</a>
            </li>
        </ul>

        <ul class="pagination">
            <li class="paginate_button active">
                <a href="#" aria-controls="datatable" data-dt-idx="0" tabindex="0">1</a>
            </li></ul>

        */


        //Creating Jquery DataTable
        $javascript =
            "var oTable;
                $(document).ready(function() {
                    oTable = $('#".$htmlTableId."').DataTable({
                            'bInfo': true,
                            'autoWidth': false,
                            'pageLength': ".$displayLength.",
                            'aLengthMenu': [[5, 10, 25, 50, 100, 1000], [5, 10, 25, 50, 100, 1000]],
                            'sPaginationType': '".$this->paginationType."',
                            'bPaginate': true, ";
        $sorting = "";
        if (!empty($this->sorting)) {
            $sorting = " 'order': [";
            $tmpSorting = "";
            foreach ($this->sorting as $value => $order) {
                if (strlen($tmpSorting) > 0) {
                    $tmpSorting .= ", ";
                }

                $tmpSorting .= "[".$value.", '".$order."']";
            }
            $sorting .= $tmpSorting . "],";
        } else {
            $sorting = " 'order': [], ";
        }
        $javascript .= $sorting;
        $javascript .=      "'aoColumnDefs': [
                                {'bSortable': false, 'aTargets': [ ";
        $columnSorting = "";
        foreach ($this->columnContainer->getColumns() as $column) {
            if (!$column->getEnableSorting()) {
                if (strlen($columnSorting) > 0) {
                    $columnSorting .= ",";
                }

                $columnSorting .= $column->getIndex();
            }
        }
        $javascript .= $columnSorting;
        $javascript .=          "]}
                            ],
                            'aoColumns': [ ";
        $sWidth = "";
        foreach ($this->columnContainer->getColumns() as $column) {
            $width = $column->getWidth();
            if (strlen($sWidth) > 0) {
                $sWidth .= ",";
            }
            if (!empty($width)) {
                $sWidth .= "{ 'sWidth': '" . $column->getWidth() . "px' }";
            } else {
                $sWidth .= "{ 'sWidth': null }";
            }
        }
        $javascript .= $sWidth;
        $javascript .=      "],
                            'sDom': \"".$this->sDom."\",
                            'bProcessing': true,
                            'bServerSide': true,
                            'sServerMethod': 'POST',
                            'sAjaxSource': '".$url."',
                            'fnServerParams': function (aoData) {
                            ";
        foreach ($this->filterContainer->getFilters() as $filter) {
            $javascript .= "aoData.push({".$filter->getJSToFilter()."});";
        }

        $javascript .=     "
                            },
                            'sCookiePrefix': 'ged_datatable_',
                            'bStateSave': ".($saveState ? "true" : "false").",
                            'iCookieDuration': 60*10,
                            'oLanguage': {
                                'sProcessing':   'Processando...',
                                'sLengthMenu':   '_MENU_',
                                'sZeroRecords':  'Não foram encontrados resultados',
                                'sInfo':         '".str_replace('_URL_', $url, $this->sInfo)."',
                                'sInfoEmpty':    '".$this->sInfoEmpty."',
                                'sInfoFiltered': '<span class=\"tooltip\" title=\"Total de Registros dentro do Filtro Selecionado\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _MAX_</span>',
                                'sInfoPostFix':  '',
                                'sInfoThousands': '.',
                                'sUrl':          '',
                                'oPaginate': {
                                    ".$this->oPaginate."
                                }
                            }".$extraParams."
                    });


                            oTable.on( 'draw', function () {
                                $(this).wrap($('<div />').addClass('table-responsive'));
                                if ($('[data-rel=\"tooltip\"]').length && $.fn.tooltip) {
                                    $('[data-rel=\"tooltip\"]').tooltip();
                                }
                            });

                 ";
        //Creating filter events
        if (!empty($clearButtonId) && !empty($filterButtonId)) {
            $javascript .= "$('#"."$filterButtonId"."').click(function(){
                oTable.draw();
            });
            ";

            $javascript .= "$('#"."$clearButtonId"."').click(function(){";

            $clearOrder = "";
            foreach ($this->columnContainer->getColumns() as $column) {
                if (!empty($clearOrder)) $clearOrder .= ", ";
                $clearOrder .= "[".$column->getIndex().", '']";
            }
            
            $javascript .= " oTable.order( ".$clearOrder." ); ";

            foreach ($this->filterContainer->getFilters() as $filter) {
                $javascript  .= $filter->getJSToClear();
            }
            $javascript .= " oTable.draw(); ";
            $javascript .= "});
             ";
        }

        $javascript .= "});
             ";

        return $javascript;
    }

    /**
     * @param $n
     */
    public function createNColumns($n)
    {
        // Always create more...
        for ($i = $this->columnContainer->count(); $i < $n; $i++) {
            $this->columnContainer->addColumn($i);
        }
    }

    /**
     * @return Column[]|array
     */
    public function getColumns()
    {
        return $this->columnContainer->getColumns();
    }

    public function setColumnWidth($index, $width)
    {
        $this->columnContainer->getColumnByIndex($index)->setWidth($width);
    }

    /**
     * @param $filter
     */
    public function setFilter($filter)
    {
        $this->filterContainer->setFilter($filter);
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filterContainer->getFilters();
    }

    /**
     * @param array|InterfaceElement $element
     * @param $columnIndex
     */
    public function insertElement($columnIndex, $element)
    {
        if (is_array($element)) {
            foreach ($element as $aElement) {
                $this->columnContainer->getColumnByIndex($columnIndex)->insertElement($aElement);
            }
        } else {
            $this->columnContainer->getColumnByIndex($columnIndex)->insertElement($element);
        }
    }

    public function clearElements($columnIndex)
    {
        $this->columnContainer->getColumnByIndex($columnIndex)->clearElements();
    }

    public function insertSeparatorInColumn($columnIndex, $separator)
    {
         $this->columnContainer->getColumnByIndex($columnIndex)->setSeparator($separator);
    }

    /**
     * @param array|int $index
     */
    public function columnEnableSorting($index)
    {
        foreach($index as $aIndex) {
            $this->columnContainer->getColumnByIndex($aIndex)->setEnableSorting(true);
        }
    }

    /**
     * @return string
     */
    public function getPaginationType()
    {
        return $this->paginationType;
    }

    /**
     * @param string $paginationType
     */
    public function setPaginationType($paginationType)
    {
        $this->paginationType = $paginationType;
    }

    /**
     * @return string
     */
    public function getSDom()
    {
        return $this->sDom;
    }

    /**
     * @param string $sDom
     */
    public function setSDom($sDom)
    {
        $this->sDom = $sDom;
    }

    /**
     * @return string
     */
    public function getOPaginate()
    {
        return $this->oPaginate;
    }

    /**
     * @param string $oPaginate
     */
    public function setOPaginate($oPaginate)
    {
        $this->oPaginate = $oPaginate;
    }

    /**
     * @return string
     */
    public function getSInfo(): string
    {
        return $this->sInfo;
    }

    /**
     * @param string $sInfo
     */
    public function setSInfo(string $sInfo)
    {
        $this->sInfo = $sInfo;
    }

    /**
     * @return string
     */
    public function getSInfoEmpty(): string
    {
        return $this->sInfoEmpty;
    }

    /**
     * @param string $sInfoEmpty
     */
    public function setSInfoEmpty(string $sInfoEmpty)
    {
        $this->sInfoEmpty = $sInfoEmpty;
    }

    /**
     * @param EventManagerInterface $events
     * @return $this
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers([
            __CLASS__,
            get_called_class(),
        ]);
        $this->events = $events;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }
}