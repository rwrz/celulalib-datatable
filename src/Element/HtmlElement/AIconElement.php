<?php
/**
 * Celula Gestão de Arquivos e Documentos
 * User: Rodrigo Boratto
 * Date: 7/15/15
 * Time: 12:15 PM
 */
namespace CelulaLib\DataTable\Element\HtmlElement;

class AIconElement extends AbstractElement
{
    /**
     * @var string
     */
    private $icon;

    /**
     * AIconElement constructor.
     * @param array $attributes
     * @param string $icon
     * @param bool|array|\Closure $requirement
     */
    function __construct(
        $attributes = array(),
        $icon = "",
        $requirement = false
    ) {
        parent::__construct($attributes, array(), $requirement);
        $this->icon = $icon;
    }

    /**
     * @param array $attributes
     * @param array $line
     * @return string
     */
    public function htmlify($attributes, $line, $actions)
    {
        //<a href="profil.html"><i class="glyph-icon flaticon-account"></i></a>
        $value = "<a ";
        $value .= $this->attributeToHtml($attributes);
        $value .= " >";
        $value .= '<i class="'.$this->icon.'"></i>';
        $value .= "</a>";

        return $value;
    }
}