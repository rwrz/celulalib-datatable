<?php

namespace CelulaLib\DataTable\Element\HtmlElement;

/**
 *
 */
class AImgElement extends AbstractElement
{
    /**
     * @var ImgElement
     */
    private $img;

    /**
     * AImgElement constructor.
     * @param array $attributesForA
     * @param array $attributesForImg
     * @param bool $requirement
     */
    function __construct(
        $attributesForA = array(),
        $attributesForImg = array(),
        $requirement = false
    ) {
        parent::__construct($attributesForA, array(), $requirement);
        $this->img = new ImgElement($attributesForImg);
    }

    protected function htmlify($attributes, $line, $actions)
    {
        $value = "<a ";
        $value .= $this->attributeToHtml($attributes);
        $value .= " >";
        $value .= $this->img->getValueToPrint($line, $actions);
        $value .= " </a>";

        return $value;
    }
}
