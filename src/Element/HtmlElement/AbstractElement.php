<?php

namespace CelulaLib\DataTable\Element\HtmlElement;

use CelulaLib\DataTable\Action\Action;
use CelulaLib\DataTable\Element\InterfaceElement;

/**
 *
 */
abstract class AbstractElement implements InterfaceElement
{
    /**
     * @var array
     */
    protected $attributes;

    /**
     * Inserted Actions by canBePrinted
     * @var Action[]
     */
    protected $actions = array();

    /**
     * Array with dbColumn and value keys, to be validated (on a equals), or a closure that returns bool
     * @var bool|array|\Closure
     */
    protected $requirement = false;

    /**
     * Actions to validate if this element is allowed to display
     * This is optional, since if the element already received an action
     * in its attributes, you don't need to specify here
     * @var array
     */
    protected $possibleActions = array();


    /**
     * AbstractElement constructor.
     * @param array $attributes
     * @param array $possibleActions
     * @param bool|array|\Closure $requirement
     */
    public function __construct($attributes = array(), $possibleActions = array(), $requirement = false)
    {
        $this->attributes = $attributes;
        $this->possibleActions = $possibleActions;
        $this->requirement = $requirement;
    }

    /**
     * @return array|bool|\Closure
     */
    public function getRequirement()
    {
        return $this->requirement;
    }

    /**
     * @param $line
     * @return array
     */
    protected function replaceAttributes($line)
    {
        $replacedAttributes = $this->attributes;

        $actions = array_merge($this->actions, $this->possibleActions);

        foreach ($actions as $action) {
            foreach ($action->getDbColumns() as $key => $columnName) {
                foreach ($replacedAttributes as $keyReplacedAttribute => $replacedAttributeValue) {
                    $replacedAttributes[$keyReplacedAttribute] = str_replace(
                        $key,
                        $line[$columnName],
                        $replacedAttributeValue
                    );
                }
            }
        }

        return $replacedAttributes;
    }

    /**
     * @param $attributes
     * @return string
     */
    protected function attributeToHtml($attributes)
    {
        $valor = "";
        foreach ($attributes as $key => $value) {
            $valor .= $key.'="'.$value.'" ';
        }
        return $valor;
    }

    protected function validateRequirement($requirement, $line)
    {
        if (is_array($requirement)) {
            if (array_key_exists("dbColumn", $requirement) && array_key_exists("value", $requirement)) {
                $dbColumn = $requirement['dbColumn'];
                $value = $requirement['value'];

                if (!array_key_exists($dbColumn, $line)) {
                    throw new \Exception("Column" . $dbColumn . " does not exists");
                }

                return ($line[$dbColumn] === $value);
            } elseif (array_key_exists("validateFunction", $requirement)) {
                $validateFunction = $requirement["validateFunction"];
                if ($validateFunction instanceof \Closure) {
                    return call_user_func($validateFunction, $line);
                } else {
                    throw new \Exception("validateFunction is not a Closure");
                }
            }
        } else {
            return !$requirement;
        }
    }

    /**
     * @param array $line
     * @param Action[] $actions
     * @return bool|mixed
     * @throws \Exception
     */
    public function canBePrinted($line, $actions)
    {
        if (empty($line)) {
            throw new \Exception("Line can not be empty!");
        }

        // SE NAO PASSAR, RETORNA FALSE, CASO CONTRARIO, VAMOS PARA AS ACTIONS
        if (!$this->validateRequirement($this->getRequirement(), $line)) {
            return false;
        }

        $this->insertActions($actions);

        if (!empty($this->possibleActions)) {
            $possibleActionIsAllowed = false;
            foreach ($this->possibleActions as $action) {
                if ($action->isAllowed() && $this->validateRequirement($action->getRequirement(), $line)) {
                    $possibleActionIsAllowed = true;
                }
            }

            return $possibleActionIsAllowed;
        }

        // Validate canBePrinted using already inserted actions
        foreach ($this->actions as $action) {
            // SE NAO PASSAR, RETORNA FALSE, CASO CONTRARIO, VAMOS PRO PROXIMO
            if (!$action->isAllowed() || !$this->validateRequirement($action->getRequirement(), $line)) {
                return false;
            }
        }

        return true;
    }

    /**
     * This method will insert actions based on attributes
     * and replace the url of those attributes - and will inject the possibleActions too
     * converting the names from the constructor to real actions
     * @param Action[] $actions
     */
    protected function insertActions($actions)
    {
        $convertedPossibleAction = array();

        // No actions inserted yet ?
        if (empty($this->actions)) {
            foreach ($actions as $action) {
                // Change attribute for real "&"
                foreach ($this->attributes as &$attribute) {
                    // found an action
                    if (stripos($attribute, '::'.$action->getName()) !== false) {
                        $this->actions[$action->getName()] = $action;
                        $attribute = str_replace('::'.$action->getName(), $action->getUrl(), $attribute);
                    }
                }

                foreach ($this->possibleActions as $possibleAction) {
                    if (is_string($possibleAction)) {
                        // found an action
                        if (stripos($possibleAction, $action->getName()) !== false) {
                            $convertedPossibleAction[$action->getName()] = $action;
                        }
                    }
                }
            }
        }

        if (!empty($convertedPossibleAction)) {
            $this->possibleActions = $convertedPossibleAction;
        }
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @param array $line
     * @return mixed
     */
    protected abstract function htmlify($attributes, $line, $actions);

    /**
     * @param $line
     * @param Action[] $actions
     * @return mixed|string
     */
    public function getValueToPrint($line, $actions)
    {
        $value = "";
        if ($this->canBePrinted($line, $actions)) {
            $value = $this->htmlify($this->replaceAttributes($line), $line, $actions);
        }
        return $value;
    }
}
