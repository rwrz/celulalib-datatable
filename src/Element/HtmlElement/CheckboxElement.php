<?php

namespace CelulaLib\DataTable\Element\HtmlElement;

/**
 *
 */
class CheckboxElement extends AbstractElement
{
    protected function htmlify($attributes, $line, $actions)
    {
        $value = "<input type='checkbox' ";
        $value .= $this->attributeToHtml($attributes);
        $value .= " />";

        return $value;
    }
}
