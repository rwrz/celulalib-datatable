<?php

namespace CelulaLib\DataTable\Element\HtmlElement;

/**
 *
 */
class ImgElement extends AbstractElement
{
    protected function htmlify($attributes, $line, $actions)
    {
        $value = "<img ";
        $value .= $this->attributeToHtml($attributes);
        $value .= " />";

        return $value;
    }
}
