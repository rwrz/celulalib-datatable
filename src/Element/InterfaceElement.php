<?php

namespace CelulaLib\DataTable\Element;
use CelulaLib\DataTable\Action\Action;

/**
 *
 */
interface InterfaceElement
{
    /**
     * @param array $line
     * @param Action[] $actions
     * @return string
     */
    public function getValueToPrint($line, $actions);
}
