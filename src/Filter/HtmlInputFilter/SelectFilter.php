<?php
/**
 * Celula Gestão de Arquivos e Documentos
 * User: Rodrigo Boratto
 * Date: 11/20/12
 * Time: 2:18 PM
 */
namespace CelulaLib\DataTable\Filter\HtmlInputFilter;


/**
 * Filtro de SELECTS
 */
class SelectFilter extends AbstractFilter
{

    /**
     * @var String
     */
    private $selectName;

    /**
     * @param string $identifier
     * @param string $selectName
     * @param string $dbTable
     * @param string $dbColumnName
     * @param int $type
     */
    public function __construct($identifier, $selectName, $dbTable, $dbColumnName, $type)
    {
        parent::__construct($identifier, $dbTable, $dbColumnName, $type);
        $this->selectName = $selectName;
    }

    /**
     * @return string
     */
    public function getJSToFilter()
    {
        $javascript = "'name': '".$this->identifier."', 'value': $(\"#".$this->selectName."\").val()";
        $javascript .= "
        ";

        return $javascript;
    }

    /**
     * @return string
     */
    public function getJSToClear()
    {
        $javascript = "
            var valor = '';
            if ($('#".$this->selectName."').attr('default-value') != undefined) {
               valor = $('#".$this->selectName."').attr('default-value');
            }
            $('#".$this->selectName."').val(valor);
            decoraSelects('#".$this->selectName."');
        ";

        return $javascript;
    }
}
