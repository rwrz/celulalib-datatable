<?php
/**
 * Celula Gestão de Arquivos e Documentos
 * User: Rodrigo Boratto
 * Date: 2/21/13
 * Time: 12:23 PM
 */

namespace CelulaLib\DataTable\Paginator\Adapter;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect as DbSelectPai;

class DbSelect extends DbSelectPai
{
    /**
     * Returns the total number of rows in the result set.
     *
     * @return integer
     */
    public function count()
    {
        if ($this->rowCount !== null) {
            return $this->rowCount;
        }

        $select = clone $this->select;

        $columns = array_values($select->getRawState(Select::COLUMNS));
        $select->reset(Select::COLUMNS);
        if ($columns[0] instanceof Expression) {
            $expression = $columns[0]->getExpression();
            if (stripos($expression, "DISTINCT") !== false) {
                $select->columns(array('c' => new Expression('COUNT('.$expression.')')));
            }
        } else {
            $select->columns(array('c' => new Expression('COUNT(1)')));
        }

        $select->reset(Select::LIMIT);
        $select->reset(Select::OFFSET);
        $select->reset(Select::ORDER);
        $select->reset(Select::GROUP);

        // get join information, clear, and repopulate without columns
        $joins = $select->getRawState(Select::JOINS);
        $select->reset(Select::JOINS);
        foreach ($joins as $join) {
            $select->join($join['name'], $join['on'], array(), $join['type']);
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();
        $row       = $result->current();

        $this->rowCount = $row['c'];

        return $this->rowCount;
    }
}
